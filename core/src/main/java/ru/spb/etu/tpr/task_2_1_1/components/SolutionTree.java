/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.spb.etu.tpr.task_2_1_1.components;

import javax.swing.*;
import java.awt.*;

public class SolutionTree extends JLabel {

	private final static int SPACEUNIT = 30;
	private TaskParms parms;

	public SolutionTree(TaskParms parms) {
		super();
		this.parms = parms;
		setMinimumSize(Constants.WORK_SIZE);
		setMaximumSize(Constants.WORK_SIZE);
		setPreferredSize(Constants.WORK_SIZE);
	}

	private void paintDefaultVFuncValues(Graphics g) {
		CellRendererPane crp = new CellRendererPane();
		JLabel context = new JLabel();
		g.drawString("0", (int) (13.5 * SPACEUNIT), (int) (0.3 * SPACEUNIT));
		context.setText("<html>-v<sub>c</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (0.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-v<sub>c</sub> + v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (2.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-2v<sub>c</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (4.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-2v<sub>c</sub> + v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (5.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-2v<sub>c</sub> + 2v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (6.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-3v<sub>c</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (7.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-3v<sub>c</sub> + v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (8.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-3v<sub>c</sub> + 2v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (10.3 * SPACEUNIT), 150, 50);
		context.setText("<html>-3v<sub>c</sub> + 3v<sub>s</sub></html>");
		crp.paintComponent(g, context, this, (int) (13.5 * SPACEUNIT), (int) (11.3 * SPACEUNIT), 150, 50);
	}

	private void paintVFuncValues(Graphics g) {
		if (parms == null) {
			paintDefaultVFuncValues(g);
			return;
		}
		Float value;
		g.drawString("0", (int) (13.5 * SPACEUNIT), (int) (0.3 * SPACEUNIT));
		value = new Float(-parms.batteriesCost);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (1.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost + parms.batteriesPrice);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (3.2 * SPACEUNIT));

		value = new Float(-parms.batteriesCost * 2);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (5.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost * 2 + parms.batteriesPrice);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (6.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost * 2 + parms.batteriesPrice * 2);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (7.2 * SPACEUNIT));

		value = new Float(-parms.batteriesCost * 3);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (8.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost * 3 + parms.batteriesPrice);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (9.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost * 3 + parms.batteriesPrice * 2);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (11.2 * SPACEUNIT));
		value = new Float(-parms.batteriesCost * 3 + parms.batteriesPrice * 3);
		g.drawString(value.toString(), (int) (13.5 * SPACEUNIT), (int) (12.2 * SPACEUNIT));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponents(g);
		// root
		g.drawRect(0, 5 * SPACEUNIT, SPACEUNIT, SPACEUNIT);
		g.drawString("D", (int) (0.4 * SPACEUNIT), (int) (5.7 * SPACEUNIT));
		// solution 1
		g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, 0);
		g.drawLine(3 * SPACEUNIT, 0, 13 * SPACEUNIT, 0);
		// solution 2
		g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, 2 * SPACEUNIT);
		g.drawLine(3 * SPACEUNIT, 2 * SPACEUNIT, 8 * SPACEUNIT, 2 * SPACEUNIT);
		g.drawOval(8 * SPACEUNIT, 2 * SPACEUNIT - (int) (SPACEUNIT / 2), SPACEUNIT, SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 2 * SPACEUNIT, 10 * SPACEUNIT, SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, SPACEUNIT, 13 * SPACEUNIT, SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 2 * SPACEUNIT, 10 * SPACEUNIT, 3 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 3 * SPACEUNIT, 13 * SPACEUNIT, 3 * SPACEUNIT);
		// solution 3
		g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, 6 * SPACEUNIT);
		g.drawLine(3 * SPACEUNIT, 6 * SPACEUNIT, 8 * SPACEUNIT, 6 * SPACEUNIT);
		g.drawOval(8 * SPACEUNIT, 6 * SPACEUNIT - (int) (SPACEUNIT / 2), SPACEUNIT, SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 6 * SPACEUNIT, 10 * SPACEUNIT, 5 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 5 * SPACEUNIT, 13 * SPACEUNIT, 5 * SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 6 * SPACEUNIT, 10 * SPACEUNIT, 7 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 7 * SPACEUNIT, 13 * SPACEUNIT, 7 * SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 6 * SPACEUNIT, 13 * SPACEUNIT, 6 * SPACEUNIT);
		// solution 4
		g.drawLine(SPACEUNIT, (int) (5.5 * SPACEUNIT), 3 * SPACEUNIT, 10 * SPACEUNIT);
		g.drawLine(3 * SPACEUNIT, 10 * SPACEUNIT, 8 * SPACEUNIT, 10 * SPACEUNIT);
		g.drawOval(8 * SPACEUNIT, 10 * SPACEUNIT - (int) (SPACEUNIT / 2), SPACEUNIT, SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 10 * SPACEUNIT, 10 * SPACEUNIT, 8 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 8 * SPACEUNIT, 13 * SPACEUNIT, 8 * SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 10 * SPACEUNIT, 10 * SPACEUNIT, 9 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 9 * SPACEUNIT, 13 * SPACEUNIT, 9 * SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 10 * SPACEUNIT, 10 * SPACEUNIT, 11 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 11 * SPACEUNIT, 13 * SPACEUNIT, 11 * SPACEUNIT);
		g.drawLine(9 * SPACEUNIT, 10 * SPACEUNIT, 10 * SPACEUNIT, 12 * SPACEUNIT);
		g.drawLine(10 * SPACEUNIT, 12 * SPACEUNIT, 13 * SPACEUNIT, 12 * SPACEUNIT);
		// solution numbers
		g.drawString("d\u2080", (int) (3.5 * SPACEUNIT), (int) (0.5 * SPACEUNIT));
		g.drawString("d\u2081", (int) (3.5 * SPACEUNIT), (int) (2.5 * SPACEUNIT));
		g.drawString("d\u2082", (int) (3.5 * SPACEUNIT), (int) (6.5 * SPACEUNIT));
		g.drawString("d\u2083", (int) (3.5 * SPACEUNIT), (int) (10.5 * SPACEUNIT));
		// lotery
		g.drawString("L\u2081", (int) (8.3 * SPACEUNIT), (int) (2.2 * SPACEUNIT));
		g.drawString("L\u2082", (int) (8.3 * SPACEUNIT), (int) (6.2 * SPACEUNIT));
		g.drawString("L\u2083", (int) (8.3 * SPACEUNIT), (int) (10.2 * SPACEUNIT));

		g.drawString("y = 0", (int) (10.5 * SPACEUNIT), (int) (1.5 * SPACEUNIT));
		g.drawString("y ≥ 1", (int) (10.5 * SPACEUNIT), (int) (3.5 * SPACEUNIT));

		g.drawString("y = 0", (int) (10.5 * SPACEUNIT), (int) (5.5 * SPACEUNIT));
		g.drawString("y = 1", (int) (10.5 * SPACEUNIT), (int) (6.5 * SPACEUNIT));
		g.drawString("y ≥ 2", (int) (10.5 * SPACEUNIT), (int) (7.5 * SPACEUNIT));

		g.drawString("y = 0", (int) (10.5 * SPACEUNIT), (int) (8.5 * SPACEUNIT));
		g.drawString("y = 1", (int) (10.5 * SPACEUNIT), (int) (9.5 * SPACEUNIT));
		g.drawString("y = 2", (int) (10.5 * SPACEUNIT), (int) (11.5 * SPACEUNIT));
		g.drawString("y = 3", (int) (10.5 * SPACEUNIT), (int) (12.5 * SPACEUNIT));

		paintVFuncValues(g);
	}

	@Override
	public void update(Graphics g) {
		paintComponent(g);
	}

}