package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.components.FixedJPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View вывода результатов решаемой задачи
 */
public class ExampleView extends JPanel{

    private final JPanel examplePanel = new FixedJPanel();
    private final JButton btnGoInputs = new JButton("Данные");
    private final JButton btnGoMenu = new JButton("Меню");

    private final ExampleViewPresenter presenter = new ExampleViewPresenter();

    public ExampleView() {
        super(null);

        examplePanel.setBorder(BorderFactory.createLineBorder(Color.black));
        examplePanel.setSize(850, 400);
        examplePanel.setLayout( null );

        JScrollPane examplePanelWrapper = new JScrollPane(examplePanel);
        examplePanelWrapper.setSize(850, 400);
        examplePanelWrapper.setLocation(20, 20);
        this.add(examplePanelWrapper);

        btnGoInputs.setSize(100, 30);
        btnGoInputs.setLocation(20, 450);
        this.add(btnGoInputs);

        btnGoMenu.setSize(100, 30);
        btnGoMenu.setLocation(770, 450);
        this.add(btnGoMenu);

        initListeners();
    }

    private void initListeners() {
        btnGoInputs.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoInputsPressed();
            }
        });

        btnGoMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoMenuPressed();
            }
        });
    }

    public JPanel getExamplePanel() {
        return examplePanel;
    }

    @Override
    public void removeAll() {
        examplePanel.removeAll();
    }

    private class ExampleViewPresenter {

        public void btnGoInputsPressed() {
            GUIManager.setActiveView(GUIManager.getInputView());
        }

        public void btnGoMenuPressed() {
            GUIManager.setActiveView(GUIManager.getMenuView());
        }
    }
}
